<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);
Route::group([ 'middleware' => ['auth']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('companies',[CompanyController::class,'index'])->name('companies');
    Route::get('add-company',[CompanyController::class,'show']);
    Route::post('insert-company',[CompanyController::class,'store']);
    Route::get('edit-company/{id}',[CompanyController::class,'show']);
    Route::get('delete-company/{id}',[CompanyController::class,'destroy']);
    Route::post('update-company',[CompanyController::class,'update']);
    
    
    Route::get('employees',[EmployeeController::class,'index'])->name('employees');
    Route::get('add-employee',[EmployeeController::class,'show']);
    Route::post('insert-employee',[EmployeeController::class,'store']);
    Route::get('edit-employee/{id}',[EmployeeController::class,'show']);
    Route::post('update-employee',[EmployeeController::class,'update']);
    Route::get('delete-employee/{id}',[EmployeeController::class,'destroy']);
});
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/link-storage', function () {
    Artisan::call('storage:link');
});