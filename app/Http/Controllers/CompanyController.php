<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Company;
use App\Models\Employee;
use Validator;
use Redirect;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
             $datas = Company::all();
//             dd($datas);
             return Datatables::of($datas)
                     ->addColumn('action', function($datas){
                         return '<a href="' . url('edit-company/'. $datas->id.'') . '"class="edit btn btn-primary btn-sm">Edit</a>'
                                 . '<a href="' .  url('delete-company/'. $datas->id.'') . '"class="edit btn btn-danger btn-sm">Delete</a>';
                     })
                     ->rawColumns(['action'])
                     ->make(true);
         } 
         return view('company.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator=  Validator::make($request->all(), [
                'name' => 'required',
            ],[
               'name' => 'Company name is Required',
           ]);
         $logo='';
          if ($files = $request->file('logo')) {
            $validator=  Validator::make($request->all(), [
                'image' => 'mimes:png,jpeg,jpg|dimensions:nim_width=100,min_height=100|max:200', //only allow this type extension file.|dimensions:ratio=1/
            ],[
               'image.mimes' => 'Please input a valid image(png,jpg,jpeg)',
               'image.dimensions' => 'Image resolution should be min_width:100px min_height:100px',
               'image.max' => 'File size should be maximum 200kb.',
           ]);
                       
        }
        if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
             }else{                         
                 $logo = $request->name.'_'.$files->getClientOriginalName();
                 $files->storeAs('public/',    $logo );
             }
         
         $company = new Company();
         $company->name = $request->name;
         $company->email = $request->email;
         $company->logo = asset('storage').'/'.$logo;
         $company->website = $request->website;
         $company->save();
         return redirect('companies')->with('success', 'Company added Succesfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        $company = Company::where('id',$id)->first();
        return view('company.form',compact('company'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $validator=  Validator::make($request->all(), [
                'name' => 'required',
            ],[
               'name' => 'Company name is Required',
           ]);
        if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
             }
         $logo='';
          if ($files = $request->file('logo')) {
            $validator=  Validator::make($request->all(), [
                'image' => 'mimes:png,jpeg,jpg|dimensions:nim_width=100,min_height=100|max:200', //only allow this type extension file.|dimensions:ratio=1/
            ],[
               'image.mimes' => 'Please input a valid image(png,jpg,jpeg)',
               'image.dimensions' => 'Image resolution should be min_width:100px min_height:100px',
               'image.max' => 'File size should be maximum 200kb.',
           ]);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
             }else{                         
                 $logo = $request->name.'_'.$files->getClientOriginalName();
                 $files->storeAs('public/',    $logo );
             }           
        }
        
         $company = Company::find($request->id);
         $company->name= $request->name;
         $company->email = $request->email;
         $company->logo = $request->logo?asset('storage').'/'.$logo:$company->logo;
         $company->website= $request->website;
         $company->update();
          return redirect('companies')->with('success', 'Company updated Succesfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Employee::where('company',$id)->delete();
       Company::destroy($id);
       
       return redirect()->back()->with('success', 'Company  deleted !');
    }
}
