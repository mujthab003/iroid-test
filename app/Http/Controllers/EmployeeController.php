<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Company;
use App\Models\Employee;
use Validator;
use Redirect;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
             $datas = Employee::select('employees.*','companies.name as company')
                     ->join('companies','companies.id','=','employees.company')
                     ->get();
//             dd($datas);
             return Datatables::of($datas)
                     ->addColumn('action', function($datas){
                         return '<a href="' . url('edit-employee/'. $datas->id.'') . '"class="edit btn btn-primary btn-sm">Edit</a>'
                                 . '<a href="' .  url('delete-employee/'. $datas->id.'') . '"class="edit btn btn-danger btn-sm">Delete</a>';
                     })
                     ->rawColumns(['action'])
                     ->make(true);
         } 
         return view('employee.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=  Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
         if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
             }
         $employee = new Employee();
         $employee->first_name = $request->first_name;
         $employee->last_name = $request->last_name;
         $employee->company = $request->company;
         $employee->email = $request->email;
         $employee->phone = $request->phone;
         $employee->save();
         
         return redirect('employees')->with('success', 'Employee added Succesfully !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        $employee = Employee::where('id',$id)->first();
         $company = Company::all();
         return view('employee.form',compact('company','employee'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator=  Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
         if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
             }
             
             $employee = Employee::find($request->id);
             $employee->first_name = $request->first_name;
             $employee->last_name = $request->last_name;
             $employee->company = $request->company;
             $employee->email = $request->email;
             $employee->phone = $request->phone;
             $employee->update();
             return redirect('employees')->with('success', 'Employee update Succesfully !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::destroy($id);
        return redirect()->back()->with('success', 'Company  deleted !');
    }
}
