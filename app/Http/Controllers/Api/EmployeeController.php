<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
class EmployeeController extends Controller
{
    public function index() {
        
        $employee = Employee::select('employees.*','companies.*')
                     ->join('companies','companies.id','=','employees.company')
                     ->get();
        
             return $employee;
    }
}
