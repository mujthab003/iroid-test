@extends('layouts.admin')
@if($employee)
@section('title', 'Edit Employee')
@else
@section('title', 'Add Employee')
@endif
@section('page-style-files')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


@stop

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @if($employee)
               <h1>Edit Employee</h1>
               @else
            <h1>Add Employee</h1>
            @endif
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
              <li class="breadcrumb-item active">@if($employee) Edit Employee @else Add Employee @endif</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
   
    <section class="content" >
        <div class="container-fluid">
          <!-- SELECT2 EXAMPLE -->
          @if($employee)
          {!! Form::open(array('url' =>'update-employee', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
          <input type='hidden' name='id' value='{{$employee->id}}'>
          @else
          {!! Form::open(array('url' =>'insert-employee', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
          @endif
           <div class="card card-info" >
            <div class="card-header">
              <h3 class="card-title">@if($employee) Edit Employee @else Add Employee @endif</h3>
  

            </div>
              
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-12 " >
                    <div class="category-head">
                     
                        
                          <div class="form-group">
                            <label>First Name</label>
                           
                           <input type="text" name="first_name" class="form-control" @if($employee) value="{{$employee->first_name }}" @else value="{{ old('first_name') }}" @endif>
                            @if($errors->has('first_name'))
                            <small class="form-text text-danger error-msg"> {{ $errors->first('first_name') }} </small>
                            @endif
                          </div> 
                        <div class="form-group">
                            <label>Last Name</label>
                           
                           <input type="text" name="last_name" class="form-control" @if($employee) value="{{$employee->last_name }}" @else value="{{ old('last_name') }}" @endif>
                            @if($errors->has('last_name'))
                            <small class="form-text text-danger error-msg"> {{ $errors->first('last_name') }} </small>
                            @endif
                          </div> 
                        <div class="form-group">
                            <label>Company</label>
                           
                           <select type="text" name="company" class="form-control" >
                               @foreach($company as $comp)
                               <option value="{{$comp->id}}" @if($employee) @if($employee->company == $comp->id) selected @endif @endif>{{$comp->name}}</option>
                               @endforeach
                        </select>
                          </div> 
                         <div class="form-group">
                            <label>Email</label>
                           
                           <input type="text" name="email" class="form-control" @if($employee) value="{{$employee->email }}" @else value="{{ old('email') }}" @endif>
                            @if($errors->has('email'))
                            <small class="form-text text-danger error-msg"> {{ $errors->first('email') }} </small>
                            @endif
                          </div>
                         
                         <div class="form-group">
                            <label>Phone</label>                           
                           <input type="text" name="phone" class="form-control" @if($employee) value="{{$employee->phone }}" @else value="{{ old('phone') }}" @endif>
                          </div>
                       
                             <button type="submit" class="btn btn-info">Save</button>
                    </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col --> 
              </div>
            </div>
            <!-- /.card-body --> 
           </div>
          <!-- /.card -->

   {{ Form::close() }}
          
           
          </div>
          
  
          
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.control-sidebar -->

<!-- ./wrapper -->

@section('page-js-script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
  @if (session('success'))
  toastr.success("{{ session('success') }}")
  @endif
  </script>
 
  
  @stop

@endsection



  








  





