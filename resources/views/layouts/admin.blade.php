<!DOCTYPE html>

<html lang="en">
<head>
    <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/gif" sizes="100x100">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>IROID- @yield('title')</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('admin-lte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin-lte/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('admin-lte/plugins/toastr/toastr.min.css')}}">
  {{--  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">  --}}
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">


  @yield('page-style-files')
  <!-- Theme style -->
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('home')}}" class="nav-link">Home</a>
      </li>
    </ul>



    <!-- Right navbar links -->
 
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/home')}}" class="brand-link">
      
      <span class="brand-text font-weight-light">IROID</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
     

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="{{url('companies')}}" class="nav-link">
              <i class="nav-icon fas fa-building" aria-hidden="true"></i>
              <p>
              Companies
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('employees')}}" class="nav-link">
            
              <i class="nav-icon fas fa-user"></i>
              <p>
              Employees
                
              </p>
            </a>
          </li>
          
          <li class="nav-item">        
            
              <!--<p>-->
                <form method="post" action="{{route('logout')}}">
                  @csrf
                <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault();
                this.closest('form').submit();">
                       <i class="nav-icon fa fa-sign-out-alt" aria-hidden="true"></i>
                       <p>
                        Log Out
                       </p>
                   </a>
                </form>
              <!--</p>-->
            
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      <!--Admin-->
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2022  All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!--<script src="{{asset('admin-lte/plugins/jquery/jquery.min.js')}}"></script>-->
<script src="{{asset('admin-lte/plugins/moment/moment.min.js')}}"></script>


<!-- Bootstrap -->
<script src="{{asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('admin-lte/dist/js/adminlte.js')}}"></script>
<script src="{{url('js/sweetalert.min.js')}}" ></script>
<script src="{{url('admin-lte/plugins/toastr/toastr.min.js')}}"></script>


  
@yield('page-js-script')


</body>
</html>
