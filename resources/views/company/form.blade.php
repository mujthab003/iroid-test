@extends('layouts.admin')
@if($company)
@section('title', 'Edit Company')
@else
@section('title', 'Add Company')
@endif
@section('page-style-files')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{url('admin-lte/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">

@stop

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @if($company)
               <h1>Edit Company</h1>
               @else
            <h1>Add Company</h1>
            @endif
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
              <li class="breadcrumb-item active">@if($company) Edit Company @else Add Company @endif</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
   
    <section class="content" >
        <div class="container-fluid">
          <!-- SELECT2 EXAMPLE -->
          @if($company)
          {!! Form::open(array('url' =>'update-company', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
          <input type='hidden' name='id' value='{{$company->id}}'>
          @else
          {!! Form::open(array('url' =>'insert-company', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
          @endif
           <div class="card card-info" >
            <div class="card-header">
              <h3 class="card-title">@if($company) Edit Company @else Add Company @endif</h3>
  

            </div>
              
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-12 " >
                    <div class="category-head">
                     
                        
                          <div class="form-group">
                            <label>Name</label>
                           
                           <input type="text" name="name" class="form-control" @if($company) value="{{$company->name }}" @else value="{{ old('name') }}" @endif>
                            @if($errors->has('name'))
                            <small class="form-text text-danger error-msg"> {{ $errors->first('name') }} </small>
                            @endif
                          </div> 
                         <div class="form-group">
                            <label>Email</label>
                           
                           <input type="text" name="email" class="form-control" @if($company) value="{{$company->email }}" @else value="{{ old('email') }}" @endif>
                          </div>
                         @if($company)@if($company->logo)
                            <img class="img-fluid chnage-image image-upload-wrap" id="show-bnr-img" alt="Responsive image" src="{{$company->logo}}">
                          @endif @endif
                           <div class="form-group">
                            <label>Logo</label>                           
                           <input type="file" name="logo" class="form-control">
                            @if($errors->has('logo'))
                            <small class="form-text text-danger error-msg"> {{ $errors->first('logo') }} </small>
                            @endif
                          </div>
                         <div class="form-group">
                            <label>Website</label>                           
                           <input type="text" name="website" class="form-control" @if($company) value="{{$company->website }}" @else value="{{ old('website') }}" @endif>
                          </div>
                       
                             <button type="submit" class="btn btn-info">Save</button>
                    </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col --> 
              </div>
            </div>
            <!-- /.card-body --> 
           </div>
          <!-- /.card -->

   {{ Form::close() }}
          
           
          </div>
          
  
          
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.control-sidebar -->

<!-- ./wrapper -->

@section('page-js-script')
<script src="{{url('admin-lte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<script>
  @if (session('success'))
  toastr.success("{{ session('success') }}")
  @endif
  </script>
 
  
  @stop

@endsection
@section('javascripts-new')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
@endsection


  








  





